// C++ code
//
#include <LiquidCrystal_PCF8574.h>
#include <Wire.h>
#include <HardwareSerial.h>
#define couleur_equipe 2
#define nb_menu 4
#define nb_strat 7
#define nb_com 9
#define nb_err 7
LiquidCrystal_PCF8574 lcd(0x27);
HardwareSerial Serial1(USART1);

//variable equipe bouton switch et led RGB
int green = PB3;
int red = PB4;
int blue = PB5;
int btn_equipe = PA11;
int btn_tirette = PA12; // lancer le match
boolean val_btn_equipe;
boolean val_btn_tirette;

// variable bouton
int btn_menu = PB0;
int btn_sous_menu = PB1;
int btn_go = PA8; // bouton envoie du message vers la rasp via port serie

// variable de menu 
int menu = 1; // variable de menu principale
int sous_menu_eq = 1; //variable de sous menu 1 pour equipe
int sous_menu1 = 1; //variable de sous menu 1 pour strategie
int sous_menu2 = 1; //variable de sous menu 2 pour commande 
// Le menu commande est compose de commande ne rentrant dans aucune categorie mais nous simplifiant les taches
// reset les variables de stategie de Talos ou eteindre le RASP de facon propre 

//affichage sur lcd
String ligne1[nb_menu]={"select equipe   ",
                        "select strategie",
                        "RASPBERRY PI    ",
                        "SCORE =         "};

String equipe_text[couleur_equipe]={"JAUNE           ",
                  	                "BLEU            "};

String strat[nb_strat]={"homologation    ",
                        "simple_jaune    ",
                        "simple_bleu     ",
                        "Demo1           ",
                        "Demo2           ",
                        "DJ2             ",
                        "DJ2_goto        "};

String commande[nb_com]={"talos reset     ",
                         "talos show      ",
                         "peripheral list ",
                         "fast load sim_bl",
                         "fast load sim_ja",
                         "strat stop      ",
                         "strat reaload   ",
                         "periph reload   ",
                         "shutdown RASP   "};

String erreur[nb_err]={"base mobile 1   ",
                      "core            ",
                      "evitement 1 LID ",
                      "evitement 2 TOF ",
                      "act plantes     ",
                      "act pan solaires",
                      "superviseur     "};


//identifiant pour la rasp / IHM
int couleur_equipe_id[couleur_equipe]={2, 1}; //identifiant de la couleur de l'equipe a selectionner
String strat_id[nb_strat]={"homologation", "simple_jaune", "simple_bleu", "Demo1", "Demo2","DJ2","DJ2_goto"}; //identifiant textuel de la strategie a programmer
int commande_id[nb_com]={0, 1, 2, 3, 4, 5, 6, 7, 8}; // differenciation de la commande a choisir pour l'envoie et l'execution sur la RASP
int code_erreur[nb_err]={2, 144, 12, 13, 123, 96, 10}; // code erreur envoyer de la RASP a l'IHM
// Pour les codes d'erreurs il s'agit des identifiants des peripheriques du robot definit par Core
// L'indice d'un element du tab de String est le meme que l'indice du tab de id
// Cela permet ainsi de ce reperer grace a l'indexation des elements

// gerer les donnee recu de la RASP decoupee en parametre et valeur grace au formatage de donnees
String RASP;
char param;
String val;

// entier qui permettent la mise a jour et l'affichage des valeurs sur l'ecran LCD
long err_int;
long score_int;
String talos_state;
String periph_list;

// detection tirette
boolean bool_tir = 0;

void setup()
{
  Serial1.begin(115200);
  Wire.begin();
  Wire.beginTransmission(0x27);
  lcd.setCursor(0,0);
  lcd.begin(16, 2);
  lcd.print("demarrage IHM");
  lcd.setBacklight(HIGH);
  delay(1000);
  lcd.clear();
  pinMode(btn_equipe, INPUT_PULLDOWN);
  pinMode(btn_menu, INPUT_PULLDOWN);
  pinMode(btn_sous_menu, INPUT_PULLDOWN);
  pinMode(btn_go, INPUT_PULLDOWN);
  pinMode(btn_tirette, INPUT_PULLUP);
  
  // setup pour la led RGB
  pinMode(red, OUTPUT);
  pinMode(blue, OUTPUT);
  pinMode(green, OUTPUT);
  
  // setup pour les attachInterrupt
  attachInterrupt(digitalPinToInterrupt(btn_menu), incr_menu, FALLING);
  attachInterrupt(digitalPinToInterrupt(btn_sous_menu), incr_sous_menu, FALLING);
  attachInterrupt(digitalPinToInterrupt(btn_go), message, FALLING);
}

void loop()
{
  //fonction principale

  // appel de la fonction qui affiche les choses sur l'ecran LCD
  affichage(); 

  // lecture du port serie
  if(Serial1.available()){ 
    RASP = Serial1.readString();
    Serial1.println(RASP);
  }
  // formatage des donnees recu sur le port serie et utlisation des valeurs dans les fonctions correspondantes
  if(sscanf(RASP.c_str(), "%c;%s", &param, &val) == 2) { // on decoupe le resultat en un parametre erreur ou score avec ca valeur correspondante
    if(param == 'e'){ // test si c'est une erreur
      err_int = val.toInt(); // si oui on affiche quel peripherique il s'agit
      err_message(); // on declenche l'affichage erreur
    } else if(param == 's'){ // test si c'est un score
      score_int = val.toInt(); //si oui on actualise la valeur du score et l'affichage se passe dans la fonction affichage
    } else if(param == 't'){ // test si c'est un score
      talos_state = val; //si oui on actualise la valeur du score et l'affichage se passe dans la fonction affichage
    }
    else if(param == 'l'){ // test si c'est un score
      periph_list = val; //si oui on actualise la valeur du score et l'affichage se passe dans la fonction affichage
    }
  }
  RASP = "";

   // test tirette lancement de match
  boolean val_btn_tirette = digitalRead(btn_tirette);
  if (bool_tir != val_btn_tirette){
    tirette_message(); // appel de la fonction tirrette_message qui envoie sur le port serie le message a executer
    bool_tir = val_btn_tirette;
  }
}


// fonction du attachInterrupt du bouton menu
void incr_menu()
{
  menu = menu%4+1;
}

// fonction du attachInterrupt du bouton sous_menu
void incr_sous_menu()
{
  switch(menu){
        case 1: //menu equipe
        break;
        case 2: //menu stratégie
        sous_menu1=sous_menu1%(nb_strat)+1;
        break;
        case 3: //menu actionneur
        sous_menu2=sous_menu2%(nb_com)+1;
        break;
        case 4: //menu score
        break;
  }
}

// fonction du attachInterrupt de bouton go il s'agit de la fonction pour l'envoie du message sur le port serie a la RASP
void message()
{
  // message pour le id de la couleur de l'equipe a envoyer (case tab_id = 3 : JAUNE et case tab_id = 4 : BLEU)
  if(menu == 1){
    // affichage et envoie sur le port serie
    Serial1.printf("strat set_team %d\r\n", couleur_equipe_id[sous_menu_eq]);
  }

  //message pour le id de la strategie a envoyer
  if(menu == 2){
    // affichage et envoie sur le port serie
    Serial1.printf("strat select \"%s\"\r\n", strat_id[sous_menu1-1].c_str());
    Serial1.printf("talos start\r\n");
  }

  //commande specifique a envoyer et executer sur la rasp
  if(menu == 3){
    // affichage et envoie sur le port serie
    if((commande_id[sous_menu2-1]) == 0){
      Serial1.printf("talos reset\r\n");
    } else if((commande_id[sous_menu2-1]) == 1){
      Serial1.printf("talos show\r\n");
    } else if((commande_id[sous_menu2-1]) == 2){
      Serial1.printf("peripheral list\r\n");
    } else if((commande_id[sous_menu2-1]) == 3){
      Serial1.printf("strat reload\r\n");
      delay(3000);
      Serial1.printf("strat set_team 1\r\n");
      Serial1.printf("strat select simple_bleu\r\n");
      Serial1.printf("talos start\r\n");
    } else if((commande_id[sous_menu2-1]) == 4){
      Serial1.printf("strat reload\r\n");
      delay(3000);
      Serial1.printf("strat set_team 2\r\n");
      Serial1.printf("strat select simple_jaune\r\n");
      Serial1.printf("talos start\r\n");
    } else if((commande_id[sous_menu2-1]) == 5){
      Serial1.printf("strat stop\r\n");
    } else if((commande_id[sous_menu2-1]) == 6){
      Serial1.printf("strat reload\r\n");
    } else if((commande_id[sous_menu2-1]) == 7){
      Serial1.printf("peripheral reload\r\n");
    } else if((commande_id[sous_menu2-1]) == 8){
      Serial1.printf("sudo shutdown now\r\n");
    }
  }
}

// affichage sur l'ecran LCD
void affichage()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(ligne1[menu-1]);
  lcd.setCursor(0,1);
  switch(menu){
    case 1: // menu equipe
    equipe();
    break;
    case 2: // menu strategie
    lcd.print(strat[sous_menu1-1]);
    break;
    case 3: // menu commande rasp
    lcd.print(commande[sous_menu2-1]);
    if(((commande_id[sous_menu2-1]) == 1) && talos_state != ""){
      lcd.clear();
      lcd.setCursor(0,1);
      lcd.print(talos_state);
      delay(3000);
    }
    else if(((commande_id[sous_menu2-1]) == 2) && periph_list != ""){
      lcd.clear();
      lcd.setCursor(0,1);
      lcd.print("??");
      delay(3000);
    }
    break;
    case 4: // menu score
    lcd.print(score_int);
  }
  delay(100); 
}

// methode pour l'affichage d'une erreur
void err_message()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("ERREUR");
  lcd.setCursor(0,1);
  switch(err_int){
    case 2: // erreur base mobile 1 (deplacement)
    lcd.print(erreur[0]);   
    break;
    case 144: // erreur Core
    lcd.print(erreur[1]);
    break;
    case 12: // erreur evitement 1 (LIDAR + Algo pilotage)
    lcd.print(erreur[2]);
    break;
    case 13: // erreur evitement 2 (TOF) 
    lcd.print(erreur[3]);
    break;
    case 123: // erreur actionneur plantes
    lcd.print(erreur[4]);
    break;
    case 96: // erreur actionneur panneau solaire
    lcd.print(erreur[5]);
    break;
    case 10: // erreur superviseur
    lcd.print(erreur[6]);
    break;
  } 
  digitalWrite(blue, LOW); // set la led RGB en rouge
  digitalWrite(green, LOW);
  digitalWrite(red, HIGH);
  delay(10000);  
}

// methode pour le changement d'equipe switch et led RGB
void equipe()
{
  boolean val_btn_equipe = digitalRead(btn_equipe);
  if(val_btn_equipe == 0){
    digitalWrite(blue, LOW);
    digitalWrite(red, HIGH);
    digitalWrite(green, HIGH);
    lcd.print(equipe_text[0]); // id 1
    sous_menu_eq = 0; // jaune
  }
  else if(val_btn_equipe == 1){
    digitalWrite(red, LOW);
    digitalWrite(green, LOW);
    digitalWrite(blue, HIGH);
    lcd.print(equipe_text[1]); // id 2
    sous_menu_eq = 1; // bleu
  }
}

void tirette_message()
{
  boolean val_btn_tirette = digitalRead(btn_tirette);
  if(val_btn_tirette == 1){
    // affichage et envoie sur le port serie
    Serial1.printf("talos tirette_on\r\n");
  }
  if(val_btn_tirette == 0){
    // affichage et envoie sur le port serie
    Serial1.printf("talos tirette_off\r\n");
  }
}